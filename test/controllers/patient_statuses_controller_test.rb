require 'test_helper'

class PatientStatusesControllerTest < ActionController::TestCase
  setup do
    @patient_status = patient_statuses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:patient_statuses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create patient_status" do
    assert_difference('PatientStatus.count') do
      post :create, patient_status: { status_name: @patient_status.status_name }
    end

    assert_redirected_to patient_status_path(assigns(:patient_status))
  end

  test "should show patient_status" do
    get :show, id: @patient_status
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @patient_status
    assert_response :success
  end

  test "should update patient_status" do
    patch :update, id: @patient_status, patient_status: { status_name: @patient_status.status_name }
    assert_redirected_to patient_status_path(assigns(:patient_status))
  end

  test "should destroy patient_status" do
    assert_difference('PatientStatus.count', -1) do
      delete :destroy, id: @patient_status
    end

    assert_redirected_to patient_statuses_path
  end
end
