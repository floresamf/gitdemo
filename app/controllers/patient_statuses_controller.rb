class PatientStatusesController < ApplicationController
  before_action :set_patient_status, only: [:show, :edit, :update, :destroy]

  # GET /patient_statuses
  # GET /patient_statuses.json
  def index
    @patient_statuses = PatientStatus.all
  end

  # GET /patient_statuses/1
  # GET /patient_statuses/1.json
  def show
  end

  # GET /patient_statuses/new
  def new
    @patient_status = PatientStatus.new
  end

  # GET /patient_statuses/1/edit
  def edit
  end

  # POST /patient_statuses
  # POST /patient_statuses.json
  def create
    @patient_status = PatientStatus.new(patient_status_params)

    respond_to do |format|
      if @patient_status.save
        format.html { redirect_to @patient_status, notice: 'Patient status was successfully created.' }
        format.json { render :show, status: :created, location: @patient_status }
      else
        format.html { render :new }
        format.json { render json: @patient_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /patient_statuses/1
  # PATCH/PUT /patient_statuses/1.json
  def update
    respond_to do |format|
      if @patient_status.update(patient_status_params)
        format.html { redirect_to @patient_status, notice: 'Patient status was successfully updated.' }
        format.json { render :show, status: :ok, location: @patient_status }
      else
        format.html { render :edit }
        format.json { render json: @patient_status.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /patient_statuses/1
  # DELETE /patient_statuses/1.json
  def destroy
    @patient_status.destroy
    respond_to do |format|
      format.html { redirect_to patient_statuses_url, notice: 'Patient status was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_patient_status
      @patient_status = PatientStatus.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def patient_status_params
      params.require(:patient_status).permit(:status_name)
    end
end
