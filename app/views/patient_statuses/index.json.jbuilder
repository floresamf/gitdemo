json.array!(@patient_statuses) do |patient_status|
  json.extract! patient_status, :id, :status_name
  json.url patient_status_url(patient_status, format: :json)
end
