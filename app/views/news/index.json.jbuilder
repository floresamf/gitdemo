json.array!(@news) do |news|
  json.extract! news, :id, :name
  json.url news_url(news, format: :json)
end
